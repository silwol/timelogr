use thiserror::Error;

#[derive(Error, Debug, PartialEq, Eq)]
pub enum Error {
    #[error("invalid date")]
    InvalidDate,

    #[error("invalid time")]
    InvalidTime,

    #[error("minutes must be below 60 if hour component is present")]
    MinutesAboveSixtyWithHourComponent,

    #[error("number out of range")]
    NumberOutOfRange,
}

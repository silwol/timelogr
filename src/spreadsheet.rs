use crate::{bail, Result};
use std::path::Path;
use time::{Date, Duration, Month, Time};

#[derive(Debug)]
pub struct SpreadsheetExport {
    data: Vec<klogr::Record>,
    start: Date,
    end: Date,
}

struct Styles {
    title: umya_spreadsheet::Style,
    headline: umya_spreadsheet::Style,
    bordered: umya_spreadsheet::Style,
    bordered_duration: umya_spreadsheet::Style,
}

impl Default for Styles {
    fn default() -> Self {
        let mut simple_border = umya_spreadsheet::Border::default();
        simple_border
            .set_border_style(umya_spreadsheet::Border::BORDER_THIN);

        let mut title = umya_spreadsheet::Style::default();
        title.get_font_mut().set_size(20.0).set_bold(true);

        let mut headline = umya_spreadsheet::Style::default();
        headline.get_font_mut().set_bold(true);
        headline.set_outer_borders(simple_border.clone());

        let mut bordered = umya_spreadsheet::Style::default();
        bordered.set_outer_borders(simple_border.clone());

        let mut bordered_duration = bordered.clone();
        let mut duration_format =
            umya_spreadsheet::NumberingFormat::default();
        duration_format.set_format_code("[hh]:mm");
        bordered_duration.set_number_format(duration_format);

        Self {
            title,
            headline,
            bordered,
            bordered_duration,
        }
    }
}

impl SpreadsheetExport {
    pub fn new(
        data: Vec<klogr::Record>,
        start: Date,
        end: Date,
    ) -> Result<Self> {
        Ok(Self { data, start, end })
    }

    pub fn write_file(&self) -> Result<()> {
        let mut book = umya_spreadsheet::new_file_empty_worksheet();
        let path = Path::new("export.xlsx");

        let sheet = match book.new_sheet("Timesheet") {
            Ok(s) => s,
            Err(e) => {
                bail!("Failed to write file: {:?}", e);
            }
        };
        self.fill_spreadsheet(sheet)?;

        match umya_spreadsheet::writer::xlsx::write(&book, path) {
            Ok(()) => Ok(()),
            Err(e) => {
                bail!("Failed to write file: {:?}", e);
            }
        }
    }

    fn fill_spreadsheet(
        &self,
        sheet: &mut umya_spreadsheet::Worksheet,
    ) -> Result<()> {
        const COL_START: u32 = 1;
        const ROW_START: u32 = 1;
        let mut row = ROW_START;
        let mut col = COL_START;

        let styles = Styles::default();

        sheet.add_merge_cells("A1:B1");

        sheet.set_value_str(col, row, "Timesheet", styles.title.clone());
        row += 2;

        sheet.set_value_str(
            col,
            row,
            "Period start",
            styles.headline.clone(),
        );
        col += 1;
        sheet.set_value_date(
            col,
            row,
            self.start,
            styles.bordered.clone(),
        );
        row += 1;

        col = COL_START;
        sheet.set_value_str(
            col,
            row,
            "Period end",
            styles.headline.clone(),
        );
        col += 1;
        sheet.set_value_date(col, row, self.end, styles.bordered.clone());
        row += 1;

        col = COL_START;
        sheet.set_value_str(
            col,
            row,
            "Overall duration",
            styles.headline.clone(),
        );
        col += 1;
        let overall_col = col;
        let overall_row = row;
        row += 2;

        col = COL_START;
        sheet.set_value_str(col, row, "Date", styles.headline.clone());
        col += 1;
        sheet.set_value_str(
            col,
            row,
            "Start time",
            styles.headline.clone(),
        );
        col += 1;
        sheet.set_value_str(col, row, "End time", styles.headline.clone());
        col += 1;
        sheet.set_value_str(col, row, "Duration", styles.headline.clone());
        col += 1;
        sheet.set_value_str(
            col,
            row,
            "Work area",
            styles.headline.clone(),
        );
        col += 1;
        sheet.set_value_str(
            col,
            row,
            "Description",
            styles.headline.clone(),
        );
        row += 1;

        let data_start_row = row;
        let mut data_end_row = row;
        for record in self.data.iter() {
            for entry in record.entries.iter() {
                col = COL_START;

                sheet.set_value_date(
                    col,
                    row,
                    record.header.date,
                    styles.bordered.clone(),
                );
                col += 1;

                match &entry.entry {
                    klogr::Entry::Range(klogr::TimeRange {
                        start,
                        end,
                    }) => {
                        match start {
                            klogr::StartTime::SameDay(t) => {
                                sheet.set_value_time(
                                    col,
                                    row,
                                    *t,
                                    styles.bordered.clone(),
                                );
                            }
                            klogr::StartTime::PreviousDay(_) => {
                                unimplemented!("start time on preivious day not implemented for spreadsheet export")
                            }
                        }
                        col += 1;
                        if let Some(end) = end {
                            match end {
                                klogr::EndTime::SameDay(t) => {
                                    sheet.set_value_time(
                                        col,
                                        row,
                                        *t,
                                        styles.bordered.clone(),
                                    );
                                }
                                klogr::EndTime::NextDay(_) => {
                                    unimplemented!("end time on next day not implemented for spreadsheet export")
                                }
                            }
                        }
                        col += 1;
                    }
                    klogr::Entry::Duration(_d) => {
                        col += 2;
                    }
                }

                sheet.set_value_duration(
                    col,
                    row,
                    entry.entry.duration().unwrap_or_default(),
                    styles.bordered.clone(),
                );

                col += 1;
                sheet.set_value_str(
                    col,
                    row,
                    entry.tags.join(", "),
                    styles.bordered.clone(),
                );

                col += 1;
                sheet.set_value_str(
                    col,
                    row,
                    &entry.summary,
                    styles.bordered.clone(),
                );

                data_end_row = row;
                row += 1;
            }
        }

        sheet.set_formula(
            overall_col,
            overall_row,
            &format!("=SUM(D{data_start_row}:D{data_end_row}"),
            styles.bordered_duration.clone(),
        );

        Ok(())
    }
}

trait DateExt {
    fn num_seconds_from_midnight(&self) -> u32;
}

impl DateExt for time::PrimitiveDateTime {
    fn num_seconds_from_midnight(&self) -> u32 {
        self.hour() as u32 * 3600
            + self.minute() as u32 * 60
            + self.second() as u32
    }
}

trait AsExcelDate {
    fn as_excel_date(&self) -> f64;
}

impl AsExcelDate for time::PrimitiveDateTime {
    fn as_excel_date(&self) -> f64 {
        // See libxlsxwriter lxw_datetime_to_excel_date()
        // TODO: needs to be refined to match the excel functionality

        let year = self.year();
        let month = match self.month() {
            Month::January => 0,
            Month::February => 1,
            Month::March => 2,
            Month::April => 3,
            Month::May => 4,
            Month::June => 5,
            Month::July => 6,
            Month::August => 7,
            Month::September => 8,
            Month::October => 9,
            Month::November => 10,
            Month::December => 11,
        };
        let day = self.day();
        let subsec =
            self.nanosecond() as f64 * 0.001f64 * 0.001f64 * 0.001f64;
        let norm = 300;

        let leap = year % 4 == 0 && (year % 100 > 0 || year % 400 == 0);
        let mdays: [i32; 12] = [
            31,
            if leap { 29 } else { 28 },
            31,
            30,
            31,
            30,
            31,
            31,
            30,
            31,
            30,
            31,
        ];
        // Convert the excel seconds to a fraction of the seconds
        // in 24 hours.
        let seconds = (self.num_seconds_from_midnight() as f64 + subsec)
            / (24f64 * 60f64 * 60f64);

        let leap = if leap { 1 } else { 0 };
        let range = year - 1900;
        let days: i32 = mdays.iter().take(month as usize).sum();
        let mut days = days + day as i32 + range * 365 + range / 4
            - range / 100
            + (range + norm) / 400
            - leap;
        // adjust for excel erroneously treating 1900 as a leap year
        if days > 59 {
            days += 1;
        }
        days as f64 + seconds
    }
}

impl AsExcelDate for Date {
    fn as_excel_date(&self) -> f64 {
        self.with_hms(0, 0, 0).unwrap().as_excel_date()
    }
}

impl AsExcelDate for Time {
    fn as_excel_date(&self) -> f64 {
        Date::from_calendar_date(1899, Month::December, 31)
            .unwrap()
            .with_time(*self)
            .as_excel_date()
    }
}

impl AsExcelDate for Duration {
    fn as_excel_date(&self) -> f64 {
        (Time::MIDNIGHT + *self).as_excel_date()
    }
}

trait SpreadsheetExt {
    fn set_value_str<S: AsRef<str>>(
        &mut self,
        col: u32,
        row: u32,
        value: S,
        style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell;

    fn set_value_date(
        &mut self,
        col: u32,
        row: u32,
        date: Date,
        style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell;

    fn set_value_time(
        &mut self,
        col: u32,
        row: u32,
        time: Time,
        style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell;

    fn set_value_duration(
        &mut self,
        col: u32,
        row: u32,
        duration: Duration,
        style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell;

    fn set_formula(
        &mut self,
        col: u32,
        row: u32,
        formula: &str,
        style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell;
}

impl SpreadsheetExt for umya_spreadsheet::Worksheet {
    fn set_value_str<S: AsRef<str>>(
        &mut self,
        col: u32,
        row: u32,
        value: S,
        style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell {
        self.get_cell_by_column_and_row_mut(&col, &row)
            .set_value(value.as_ref())
            .with_style(style)
    }

    fn set_value_date(
        &mut self,
        col: u32,
        row: u32,
        date: Date,
        mut style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell {
        let mut format = umya_spreadsheet::NumberingFormat::default();
        format.set_format_code("yyyy-mm-dd");

        let mut value = umya_spreadsheet::CellValue::default();
        value.set_value_from_numberic(date.as_excel_date());
        let cell = self
            .get_cell_by_column_and_row_mut(&col, &row)
            .set_cell_value(value);
        style.set_number_format(format);
        cell.set_style(style);
        cell
    }

    fn set_value_time(
        &mut self,
        col: u32,
        row: u32,
        time: Time,
        mut style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell {
        let mut format = umya_spreadsheet::NumberingFormat::default();
        format.set_format_code("hh:mm");

        let mut value = umya_spreadsheet::CellValue::default();
        value.set_value_from_numberic(time.as_excel_date());
        let cell = self
            .get_cell_by_column_and_row_mut(&col, &row)
            .set_cell_value(value);
        style.set_number_format(format);
        cell.set_style(style);
        cell
    }

    fn set_value_duration(
        &mut self,
        col: u32,
        row: u32,
        duration: Duration,
        mut style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell {
        let mut format = umya_spreadsheet::NumberingFormat::default();
        format.set_format_code("[hh]:mm");

        let mut value = umya_spreadsheet::CellValue::default();
        value.set_value_from_numberic(duration.as_excel_date());
        let cell = self
            .get_cell_by_column_and_row_mut(&col, &row)
            .set_cell_value(value);
        style.set_number_format(format);
        cell.set_style(style);
        cell
    }

    fn set_formula(
        &mut self,
        col: u32,
        row: u32,
        formula: &str,
        style: umya_spreadsheet::Style,
    ) -> &mut umya_spreadsheet::Cell {
        self.get_cell_by_column_and_row_mut(&col, &row)
            .set_formula(formula)
            .set_style(style)
    }
}

trait CellExt {
    fn with_style(&mut self, style: umya_spreadsheet::Style) -> &mut Self;
}

impl CellExt for umya_spreadsheet::Cell {
    fn with_style(&mut self, style: umya_spreadsheet::Style) -> &mut Self {
        self.set_style(style);
        self
    }
}

trait StyleExt {
    fn set_outer_borders(
        &mut self,
        border: umya_spreadsheet::Border,
    ) -> &mut Self;
}

impl StyleExt for umya_spreadsheet::Style {
    fn set_outer_borders(
        &mut self,
        border: umya_spreadsheet::Border,
    ) -> &mut Self {
        self.get_borders_mut()
            .set_top(border.clone())
            .set_left(border.clone())
            .set_right(border.clone())
            .set_bottom(border.clone());
        self
    }
}

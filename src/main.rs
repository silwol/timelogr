mod jira;
mod pmsmart;
mod spreadsheet;

use anyhow::{bail, Context, Result};
use clap::Parser;
use directories::ProjectDirs;
use jira::JiraWeekTableSeries;
use pmsmart::PmSmartWeekTableSeries;
use regex::Regex;
use serde_derive::{Deserialize, Serialize};
use spreadsheet::SpreadsheetExport;
use std::{collections::BTreeMap, path::PathBuf, str::FromStr};
use time::{Date, Duration, Weekday};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
enum ExportFormat {
    PmSmart,
    Jira,
    Spreadsheet,
}

impl FromStr for ExportFormat {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self> {
        match s {
            "pmsmart" => Ok(ExportFormat::PmSmart),
            "jira" => Ok(ExportFormat::Jira),
            "spreadsheet" => Ok(ExportFormat::Spreadsheet),
            other => bail!("Unknown export format {:?}", other),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Config {
    projects: BTreeMap<String, Project>,
    tags: BTreeMap<String, TagConfiguration>,
}

impl Config {
    fn load() -> Result<Self> {
        let project_dirs =
            ProjectDirs::from("net", "silwol", "timelogr").unwrap();
        let config_path = project_dirs.config_dir().join("config.toml");

        let s =
            std::fs::read_to_string(&config_path).with_context(|| {
                format!("Couldn't load config from {:?}", &config_path)
            })?;

        Ok(toml::from_str(&s)?)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Project {
    name: String,
    packages: BTreeMap<String, WorkPackage>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct WorkPackage {
    name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TagConfiguration {
    id: WorkPackageIdentifier,

    #[serde(default)]
    pmsmart: PmSmartTagConfiguration,

    #[serde(default)]
    jira: JiraTagConfiguration,
}

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
struct PmSmartTagConfiguration {
    #[serde(default)]
    insert_tag: bool,
}

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
struct JiraTagConfiguration {
    #[serde(default)]
    show: bool,
}

#[derive(
    Debug, Clone, Serialize, Deserialize, PartialOrd, Ord, PartialEq, Eq,
)]
pub struct WorkPackageIdentifier {
    project: String,
    package: String,
}

trait GetDateBounds {
    fn get_bounds(&self) -> Option<(Date, Date)>;
}

impl GetDateBounds for Vec<klogr::Record> {
    fn get_bounds(&self) -> Option<(Date, Date)> {
        let mut bounds = None;

        for record in self.iter() {
            bounds = match bounds {
                Some((start, end)) => {
                    let start = std::cmp::min(record.header.date, start);
                    let end = std::cmp::max(record.header.date, end);
                    Some((start, end))
                }
                None => Some((record.header.date, record.header.date)),
            };
        }
        bounds
    }
}

trait FilterRange {
    fn filter_range(self, start: Option<Date>, end: Option<Date>) -> Self;
}

impl FilterRange for Vec<klogr::Record> {
    fn filter_range(self, start: Option<Date>, end: Option<Date>) -> Self {
        fn is_in_range(
            record: &klogr::Record,
            start: Option<Date>,
            end: Option<Date>,
        ) -> bool {
            match (start, end) {
                (Some(start), Some(end)) => {
                    record.header.date >= start
                        && record.header.date <= end
                }
                (Some(start), None) => record.header.date >= start,
                (None, Some(end)) => record.header.date <= end,
                (None, None) => true,
            }
        }
        self.into_iter()
            .filter(|r| is_in_range(r, start, end))
            .collect()
    }
}

trait FilterTagPattern: Sized {
    fn filter_tag_pattern(self, pattern: &str) -> Result<Self>;
}

impl FilterTagPattern for Vec<klogr::Record> {
    fn filter_tag_pattern(self, pattern: &str) -> Result<Self> {
        let regex = Regex::new(pattern)?;

        fn filter_by_tag_pattern(
            mut record: klogr::Record,
            pattern: &Regex,
        ) -> Option<klogr::Record> {
            record.entries.retain(|e| {
                println!("TAGS: {:?}", e.tags);
                println!("PATTERN: {:?}", pattern);
                println!(
                    "IS_MATCH: {}",
                    e.tags.iter().any(|tag| pattern.is_match(tag))
                );
                println!();
                e.tags.iter().any(|tag| pattern.is_match(tag))
            });

            if record.entries.is_empty() {
                println!("NONE");
                println!();
                None
            } else {
                println!("SOME");
                println!();
                Some(record)
            }
        }

        Ok(self
            .into_iter()
            .filter_map(|r| filter_by_tag_pattern(r, &regex))
            .collect())
    }
}

#[derive(Debug, Parser)]
#[clap(name = "timelogr", about = "Opinionated klog file analyzer")]
struct Args {
    file: PathBuf,
    format: ExportFormat,
    pattern: String,
    #[clap(long, parse(try_from_str = parse_date))]
    start: Option<Date>,
    #[clap(long, parse(try_from_str = parse_date))]
    end: Option<Date>,
}

fn parse_date(s: &str) -> Result<Date> {
    let format = time::format_description::parse("[year]-[month]-[day]")?;
    let d = Date::parse(s, &format)?;
    Ok(d)
}

impl Args {
    fn process(self, config: &Config) -> Result<()> {
        let file_str = std::fs::read_to_string(&self.file)?;

        let parsed = klogr::parse(&file_str)?;

        let filtered = parsed
            .filter_range(self.start, self.end)
            .filter_tag_pattern(&self.pattern)?;

        let (found_start, found_end) = filtered
            .get_bounds()
            .context("no data found in time range")?;

        let start = self.start.unwrap_or(found_start);
        let end = self.end.unwrap_or(found_end);

        match self.format {
            ExportFormat::PmSmart => {
                let tables = PmSmartWeekTableSeries::new(
                    &config.projects,
                    &config.tags,
                    &filtered,
                    start,
                    end,
                )?;
                for table in tables.tables() {
                    table.pretty_table().print_tty(false)?;
                    println!();
                }
            }
            ExportFormat::Jira => {
                let table = JiraWeekTableSeries::new(
                    &config.projects,
                    &config.tags,
                    &filtered,
                    start,
                    end,
                )?;
                for table in table.tables() {
                    table.pretty_table().print_tty(false)?;
                }
            }
            ExportFormat::Spreadsheet => {
                let export = SpreadsheetExport::new(filtered, start, end)?;
                export.write_file()?;
            }
        }

        Ok(())
    }
}

trait SplitWeekRanges {
    fn split_week_ranges(&self) -> Vec<(Date, Date)>;
}

impl SplitWeekRanges for (Date, Date) {
    fn split_week_ranges(&self) -> Vec<(Date, Date)> {
        let mut ranges = Vec::new();

        let mut first_day = self.0;
        while first_day.weekday() != Weekday::Monday {
            first_day -= Duration::days(1);
        }

        let mut last_day = self.1;
        while last_day.weekday() != Weekday::Sunday {
            last_day += Duration::days(1);
        }

        let mut current = first_day;
        let mut current_range_start = first_day;

        while current <= last_day {
            if current.weekday() == Weekday::Sunday {
                ranges.push((current_range_start, current));
            }
            if current.weekday() == Weekday::Monday {
                current_range_start = current;
            }
            current += Duration::days(1);
        }
        ranges
    }
}

trait GetDays {
    fn get_days(&self) -> Vec<Date>;
}

impl GetDays for (Date, Date) {
    fn get_days(&self) -> Vec<Date> {
        let mut f = Vec::new();
        let mut current = self.0;
        while current <= self.1 {
            f.push(current);
            current += Duration::days(1);
        }
        f
    }
}

fn main() -> Result<()> {
    let args = Args::from_args();

    let config = Config::load()?;

    args.process(&config)
}

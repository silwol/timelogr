use crate::{GetDays, Project, Result, SplitWeekRanges, TagConfiguration};
use anyhow::{bail, Context};
use prettytable::{Cell, Row, Table};
use std::collections::{BTreeMap, BTreeSet};
use time::{Date, Duration};

pub struct PmSmartWeekTableSeries {
    tables: Vec<PmSmartTable>,
}

impl PmSmartWeekTableSeries {
    pub fn new(
        projects: &BTreeMap<String, Project>,
        tags: &BTreeMap<String, TagConfiguration>,
        data: &Vec<klogr::Record>,
        start: Date,
        end: Date,
    ) -> Result<Self> {
        let ranges = (start, end).split_week_ranges();

        let mut tables = Vec::new();
        for (start, end) in ranges {
            let table =
                PmSmartTable::new(projects, tags, data, start, end)?;
            tables.push(table);
        }
        Ok(PmSmartWeekTableSeries { tables })
    }

    pub fn tables(&self) -> &Vec<PmSmartTable> {
        &self.tables
    }
}

pub struct PmSmartTable {
    start: Date,
    end: Date,
    rows: Vec<PmSmartTableRow>,
}

impl PmSmartTable {
    fn new(
        projects: &BTreeMap<String, Project>,
        tags: &BTreeMap<String, TagConfiguration>,
        data: &Vec<klogr::Record>,
        start: Date,
        end: Date,
    ) -> Result<Self> {
        let mut workpackages = BTreeMap::new();

        for record in data.iter() {
            for line in record.entries.iter() {
                if line.tags.len() != 1 {
                    bail!(
                        "{} entry has more than one tag",
                        record.header.date
                    );
                }
                let tag = line.tags[0].to_string();
                let tag_config = tags.get(&tag).context(format!(
                    "no config entry for tag {} found",
                    tag
                ))?;
                let duration = line
                    .entry
                    .duration()
                    .context(format!("unknown duration found"))?;

                if duration < Duration::ZERO {
                    bail!("negative duration found");
                }

                let packageentry = workpackages
                    .entry(&tag_config.id)
                    .or_insert(BTreeMap::new());

                let dayentry = packageentry
                    .entry(record.header.date)
                    .or_insert((BTreeSet::new(), Duration::ZERO));
                if line.summary != "" {
                    let description = if tag_config.pmsmart.insert_tag {
                        format!("{} ({})", line.summary, tag)
                    } else {
                        line.summary.to_string()
                    };
                    dayentry.0.insert(description);
                }
                dayentry.1 = dayentry.1 + duration;
            }
        }

        let mut rows = Vec::new();

        for (id, days) in workpackages {
            let project = projects
                .get(&id.project)
                .context(format!("no such project: {:?}", id.project))?;
            let workpackage =
                project.packages.get(&id.package).context(format!(
                    "no workpackage {:?} in project {:?}",
                    id.package, id.project
                ))?;

            rows.push(PmSmartTableRow {
                project_id: id.project.to_string(),
                project_name: project.name.to_string(),
                workpackage_id: id.package.to_string(),
                workpackage_name: workpackage.name.to_string(),
                workdays: days,
            });
        }

        Ok(PmSmartTable { start, end, rows })
    }

    fn dates(&self) -> Vec<Date> {
        (self.start, self.end).get_days()
    }

    fn dates_str(&self) -> Vec<String> {
        self.dates()
            .into_iter()
            .map(|d| format!("{}\n{}", d, d.weekday()))
            .collect()
    }

    fn headers(&self) -> Vec<String> {
        let mut f =
            vec!["Projekt".to_string(), "Arbeitspaket".to_string()];
        f.append(&mut self.dates_str());
        f
    }

    fn pretty_headers(&self) -> Row {
        Row::new(self.headers().iter().map(|h| Cell::new(h)).collect())
    }

    fn pretty_rows(&self) -> Vec<Row> {
        std::iter::once(self.pretty_headers())
            .chain(
                self.rows.iter().map(|r| r.pretty_table_row(self.dates())),
            )
            .collect()
    }

    pub fn pretty_table(&self) -> Table {
        Table::init(self.pretty_rows())
    }
}

struct PmSmartTableRow {
    project_id: String,
    project_name: String,
    workpackage_id: String,
    workpackage_name: String,
    workdays: BTreeMap<Date, (BTreeSet<String>, Duration)>,
}

impl PmSmartTableRow {
    fn durations(
        &self,
        dates: Vec<Date>,
    ) -> Vec<(BTreeSet<String>, Duration)> {
        dates
            .iter()
            .map(|d| {
                self.workdays
                    .get(d)
                    .cloned()
                    .unwrap_or((BTreeSet::new(), Duration::ZERO))
            })
            .collect()
    }

    fn durations_str(&self, dates: Vec<Date>) -> Vec<String> {
        self.durations(dates)
            .into_iter()
            .map(|d| {
                if d.1.is_zero() {
                    "".to_string()
                } else {
                    format!(
                        "{:02}:{:02}\n{}",
                        d.1.whole_hours(),
                        d.1.whole_minutes() - d.1.whole_hours() * 60,
                        d.0.into_iter()
                            .collect::<Vec<String>>()
                            .join("\n")
                    )
                }
            })
            .collect()
    }

    fn tabledata(&self, dates: Vec<Date>) -> Vec<String> {
        let mut f = vec![
            format!("{}\n{}", self.project_id, self.project_name),
            format!("{}\n{}", self.workpackage_id, self.workpackage_name),
        ];
        f.append(&mut self.durations_str(dates));
        f
    }

    fn pretty_table_row(&self, dates: Vec<Date>) -> Row {
        Row::new(
            self.tabledata(dates)
                .into_iter()
                .map(|s| Cell::new(&s))
                .collect(),
        )
    }
}

use crate::{GetDays, Project, Result, SplitWeekRanges, TagConfiguration};
use anyhow::{bail, Context};
use klogr::{EndTime, Entry, StartTime};
use prettytable::{Cell, Row, Table};
use std::collections::BTreeMap;
use time::{Date, Duration, Time};

#[derive(Debug)]
pub struct JiraWeekTableSeries {
    tables: Vec<JiraTable>,
}

impl JiraWeekTableSeries {
    pub fn new(
        projects: &BTreeMap<String, Project>,
        tags: &BTreeMap<String, TagConfiguration>,
        data: &Vec<klogr::Record>,
        start: Date,
        end: Date,
    ) -> Result<Self> {
        let ranges = (start, end).split_week_ranges();

        let mut tables = Vec::new();
        for (start, end) in ranges {
            let table = JiraTable::new(projects, tags, data, start, end)?;
            tables.push(table);
        }
        Ok(JiraWeekTableSeries { tables })
    }

    pub fn tables(&self) -> &Vec<JiraTable> {
        &self.tables
    }
}

#[derive(Debug)]
pub struct JiraTable {
    start: Date,
    end: Date,
    row: JiraTableRow,
}

impl JiraTable {
    fn new(
        _projects: &BTreeMap<String, Project>,
        tags: &BTreeMap<String, TagConfiguration>,
        data: &Vec<klogr::Record>,
        start: Date,
        end: Date,
    ) -> Result<Self> {
        let mut workdays = BTreeMap::new();

        for date in (start, end).get_days() {
            let day = workdays.entry(date).or_insert(Vec::new());

            for record in data {
                if record.header.date == date {
                    for line in record.entries.iter() {
                        if line.tags.len() != 1 {
                            bail!(
                                "{} entry has more than one tag",
                                record.header.date
                            );
                        }
                        let tag = line.tags[0].to_string();
                        let tag_config =
                            tags.get(&tag).context(format!(
                                "no config entry for tag {} found",
                                tag
                            ))?;

                        if tag_config.jira.show {
                            let (range, duration) =
                                Self::get_start_end_duration(&line.entry)?;

                            day.push(JiraWorkItem {
                                range,
                                duration,
                                tag,
                            });
                        }
                    }
                }
            }
        }

        Ok(JiraTable {
            start,
            end,
            row: JiraTableRow { workdays },
        })
    }

    fn dates_str(&self) -> Vec<String> {
        self.dates()
            .into_iter()
            .map(|d| format!("{}\n{}", d, d.weekday()))
            .collect()
    }

    fn headers(&self) -> Vec<String> {
        self.dates_str()
    }

    fn pretty_headers(&self) -> Row {
        Row::new(self.headers().iter().map(|h| Cell::new(h)).collect())
    }

    fn pretty_rows(&self) -> Vec<Row> {
        std::iter::once(self.pretty_headers())
            .chain(
                std::iter::once(&self.row).map(|r| r.pretty_table_row()),
            )
            .collect()
    }

    fn get_start_end_duration(
        entry: &Entry,
    ) -> Result<(Option<(Time, Time)>, Duration)> {
        match entry {
            Entry::Duration(d) => Ok((None, *d)),
            Entry::Range(r) => {
                let start = match r.start {
                    StartTime::SameDay(t) => t,
                    StartTime::PreviousDay(_) => {
                        bail!("no support for ranges over multiple days")
                    }
                };
                let end = match r.end {
                    Some(EndTime::SameDay(t)) => t,
                    Some(EndTime::NextDay(_)) => {
                        bail!("no support for ranges over multiple days")
                    }
                    None => bail!(
                        "can't work with time range \
                        that has an unknown end time"
                    ),
                };
                let duration = end - start;
                Ok((Some((start, end)), duration))
            }
        }
    }

    pub fn pretty_table(&self) -> Table {
        Table::init(self.pretty_rows())
    }

    fn dates(&self) -> Vec<Date> {
        let mut f = Vec::new();
        let mut current = self.start;
        while current <= self.end {
            f.push(current);
            current += Duration::days(1);
        }
        f
    }
}

#[derive(Debug)]
struct JiraTableRow {
    workdays: BTreeMap<Date, Vec<JiraWorkItem>>,
}

impl JiraTableRow {
    fn pretty_table_row(&self) -> Row {
        Row::new(
            self.workdays
                .values()
                .map(|items| {
                    let items = items
                        .iter()
                        .map(|i| i.to_string())
                        .collect::<Vec<String>>();
                    Cell::new(&items.join("\n\n"))
                })
                .collect(),
        )
    }
}

#[derive(Debug)]
struct JiraWorkItem {
    range: Option<(Time, Time)>,
    duration: Duration,
    tag: String,
}

impl JiraWorkItem {
    fn to_string(&self) -> String {
        let duration = format!(
            "{:02}:{:02}",
            self.duration.whole_hours(),
            self.duration.whole_minutes()
                - self.duration.whole_hours() * 60
        );
        let time = match self.range {
            Some((start, end)) => {
                let format =
                    time::format_description::parse("[hour]:[minute]")
                        .unwrap();
                format!(
                    "{} ({} - {})",
                    duration,
                    start.format(&format).unwrap(),
                    end.format(&format).unwrap(),
                )
            }
            None => duration,
        };
        format!("{}\n{}", time, self.tag)
    }
}
